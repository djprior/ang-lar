<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
  return View::make('todos.index');
});


Route::get('login','SessionsController@create');
Route::get('logout','SessionsController@destroy');
Route::resource('sessions', 'SessionsController');
Route::get('photos','PhotosController@create');
Route::resource('photos', 'PhotosController');
Route::resource('tweets', 'TweetsController');
Route::resource('todos', 'TodosController'); 
Route::get('admin', function()
{
  return 'This is the Admin Page';
})->before('auth');

  //Route::resource('todos', 'TodosController');