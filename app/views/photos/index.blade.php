@extends('layouts.scaffold')

@section('main')

<h1>Login</h1>

{{ Form::open(array('route' => 'photos.store','files'=>true)) }}
	<ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('fileName', 'Upload:') }}
            {{ Form::file('fileName') }}
        </li>

		<li>
			{{ Form::submit('Upload Photo', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}
@stop


