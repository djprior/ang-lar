@extends('layouts.scaffold')

@section('main')

<h1>Login</h1>
<h1>{{\Carbon\Carbon::now()}}</h1>
<h1>{{\Carbon\Carbon::now()->addDays(4)}}</h1>
<h1>{{\Carbon\Carbon::now()}}</h1>

{{ Form::open(array('route' => 'sessions.store')) }}
	<ul>
        <li>
            {{ Form::label('email', 'Email:') }}
            {{ Form::text('email') }}
        </li>

        <li>
            {{ Form::label('password', 'Password:') }}
            {{ Form::password('password') }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}
@stop


