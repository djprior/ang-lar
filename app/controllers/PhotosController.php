<?php
use Carbon\Carbon;
class PhotosController extends BaseController{

 
   public function index()
  {  
      return View::make('photos.create');  
  }
  public function create()
  {  
    return View::make('photos.create');  
  }
 	public function store()
	{
    $input =Input::all();
    $fileName=$input['fileName']->getClientOriginalName();
    $image=Image::make($input['fileName']->getRealPath());
    $image->save(public_path().'/images/'.$fileName)
      ->greyscale()
      ->save(public_path().'/images/grey-'.$fileName);
	}
    public function destroy()
  {
      
  }
}