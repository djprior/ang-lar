<?php
use Carbon\Carbon;
class 	SessionsController extends BaseController{

  public function create()
  {
    if(Auth::check()){
      return Redirect::to('/admin');
    }
    return Carbon::now()->addDays(4);
  }
  public function store()
  {
      if(Auth::attempt(Input::only('email','password')))
      {
         return Auth::user();
      }
    return Redirect::back()->withInput();
  }
    public function destroy()
  {
      Auth::logout();
      return Redirect::route('sessions.create');
  }
}